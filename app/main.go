package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
	"encoding/json"
)

var broken = false
var breakStart time.Time 

type HelloData struct {
	Version string `json:"Version"`
	Time string `json:"Time"`
	Hostname string `json:"Hostname"`
	Pod string `json:"Podname"`
}


func main() {
	// register hello function to handle all requests
	mux := http.NewServeMux()
	mux.HandleFunc("/", hello)
	mux.HandleFunc("/setBroken", setBroken)

	// use PORT environment variable, or default to 8080
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	// start the web server on port and accept requests
	log.Printf("Server listening on port %s", port)
	log.Fatal(http.ListenAndServe(":"+port, mux))
}

// hello responds to the request with a plain-text "Hello, world" message.
func hello(w http.ResponseWriter, r *http.Request) {
	log.Printf("Serving request: %s", r.URL.Path)
	host, pod := getRuntimeDetails()

	if(broken){
		if(time.Now().Sub(breakStart) > 120 * time.Second ){
			broken = false
		}else{
		  error(w, r, host, pod)
		  return
		}
	}
	
	hd := HelloData{Version:"1", Hostname: host, Pod:pod, Time: time.Now().Format("Mon Jan _2 2006 15:04:05.000")}
	
	json.NewEncoder(w).Encode(hd)

}

func getRuntimeDetails() (host string, pod string){
	host = os.Getenv("POD_NODE_NAME")
	pod, _ = os.Hostname()
	if host == "" {
	  host = pod
	  pod = "NA"
	}
	return host, pod
	
} 

func setBroken(w http.ResponseWriter, r *http.Request){
	log.Printf("Setting error to %t", !broken)
	broken = !broken
	if (broken){
		breakStart = time.Now()
	}
	host, pod := getRuntimeDetails()
	fmt.Fprintf(w, "Host: %s - Pod: %s set to Broken State: %t", host, pod, broken)
}


func error(w http.ResponseWriter, r *http.Request, host string, pod string){
	w.WriteHeader(http.StatusInternalServerError)
	fmt.Fprintf(w, "Host: %s - Pod: %s 500 Error - Something bad Happened!", host, pod)
}


