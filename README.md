<H1>Istio Circuit Breaking Demo</H1>
<H2>About this demo</H2>
The purpose of this demo is to demonstrate Istio circuit breaking via destination rules for services experiencing internal issues. The demonstration is done by deploying a golang app which can bet set to return 500 errors for a 2 minute duration. Repeatedly calling the app will trigger the destination  to open  the circuit of broken replica for duration of 3 minutes. This is demonstrated in both single and multiple replica scenarios. 

_Note For a demonstration of load based circuit breaking see the primary Istio documentation [here](https://istio.io/latest/docs/tasks/traffic-management/circuit-breaking/)._

**Project Layout**

This project contains 2 directories:
- app - source of golang app that can be set to return 500 errors
- config - kubernetes config yamls used to deploy and configure demonstration deployment

<H2>How to use this demo</H2>

**Prequisites**
- Kubernetes Cluster
- Istio or Anthos Service Mesh
- Sidecar Injection enabled for default namespace (or namespace to be used) (Istio `kubectl label namespace default istio-injection=enabled`, Anthos Service Mesh 1.7 on GKE see [Anthos Service Mesh Docs](https://cloud.google.com/service-mesh/docs/scripted-install/gke-asm-onboard-1-7#enabling_auto-injection)

<H3>Single Replica Circuit Breaking</H3>

**Steps**

1. Deploy client `client.yaml`
1. Deploy Single replica service `single-service.yaml`
1. Apply the circuit breaking rule `cb-dest-rule.yaml`
1. ssh into client container of client pod `kubectl exec -it -c client client -- /bin/bash`
    - curl cb-hello to verify connectivity
    - curl cb-hello/setBroken to set the state of the service to setBroken
    - curl cb-hello and observe 500 error returned
    - curl cb-hello until you stop seeing 500 errors and instead see "no healthy upstream" this is the circuit breaker flipping to open
    - wait 5 mins for service to reset to working and circuit breaker to close
    - curl cb-hello and you should see a suceessful return
1. exit from pod ssh session

<H3>Multi (2) Replica Circuit Breaking</H3>

**Steps**

1. If not already done deploy client `client.yaml`
1. Deploy multi replica service `multi-service.yaml`
1. If not already done apply the circuit breaking rule `cb-dest-rule.yaml`
1. ssh into client container of client pod `kubectl exec -it -c client client -- /bin/bash`
    - curl cb-hello a couple of times to verify connectivity and that 2 distint replicas respond
    - curl cb-hello/setBroken to set the state of one of the replicals to broken
    - curl cb-hello until you stop seeing 500 errors from one of the replicas and instead see only responses from the health replica (this is the circuit breaker flipping to open)
    - wait 5-7 mins for broken replica to reset to working and circuit breaker to close
    - curl cb-hello and you should again see a suceessful returned from 2 distinct replicas
1. exit from pod ssh session


